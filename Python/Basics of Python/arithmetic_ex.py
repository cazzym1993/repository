# addition example <- this line with the hashtag is a comment, it is not read by the interpreter
# "Addition" is in the print function just so that the output is nice and neat
print("Addition: 2 + 3 =")    # print what operation will be done (note, changing numbers here doesn't change the math that's done)
print(2 + 3)                  # do the math
print()                       # create an empty line

# subtraction example
print("Subtraction: 5 - 4 =")
print(5 - 4)
print()

# multiplication example
print("Multiplication: 3 * 4 =")
print(3 * 4)
print()

# division example
print("Division: 22 / 7 =")
print(22 / 7)
print()

# exponent example
print("Exponent: 3 ** 2 =")
print(3**2)
print()
