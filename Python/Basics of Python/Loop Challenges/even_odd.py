# Challenge 3
# Ask the user for a maximum number and whether you want the increments to that number to be odd or even; 
# then print all the appropriate numbers up until the maximum number
# Hint: google “python arithmetic operators” and look for "Modulus"


maxnum = input("Maximum number: ")
choice = input("Even or odd: ")

# set the modulo check number
if choice == "even": c = 0
else: c = 1

for i in range(int(maxnum)):
    if (i % 2 == c): 
        print(i)
