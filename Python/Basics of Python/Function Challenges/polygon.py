# Challenge 2
# Create a function that takes the number of sides that you want your shape to have (as well as the lengths of the sides) as an input 
# and use it to draw a regular polygon with the dimensions that you have specified.


import turtle

t = turtle.Turtle()
t.width(3)

def polygon(n_sides, length):
    for i in range(n_sides):
        t.forward(length)
        t.left(360/n_sides)

polygon(13, 80)

input()
