# Challenge 1
# Make a function that takes a number as an input and 
# returns with an answer stating whether the number is odd or even

def oddeven(x):
    if x % 2 == 0: return "even"
    else: return "odd"

while(True):
    i = int(input("Number: "))
    print(oddeven(i))
